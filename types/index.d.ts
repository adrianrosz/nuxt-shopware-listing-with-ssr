export interface iProductBox {
    id: string,
    name: string,
    desc: string,
    price: string,
}
