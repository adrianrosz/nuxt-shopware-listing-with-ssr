// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  runtimeConfig: {
    API_KEY: process.env.API_KEY,
    API_URL: process.env.API_URL,
  },
})
