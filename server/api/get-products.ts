import {iProductBox} from '~/types';

// any :)
const mapperProducts = (rawProductsFromApi: any): iProductBox[] => {
    const productsArray: iProductBox[] = [];

    for(const product of rawProductsFromApi) {
        productsArray.push({
            name: product.translated.name,
            desc: product.translated.description,
            price: String(product.calculatedPrice.totalPrice),
            id: product.id
        })
    }

    return productsArray;
}

export default defineEventHandler( async(event) => {
    const config = useRuntimeConfig();

    const URL_CATEGORY = `${config.API_URL}/product-listing/e435c9763b0d44fcab67ea1c0fdb3fa0`;
    const URL_SEARCH = `${config.API_URL}/search`;

    const { search, sort } = getQuery(event);
    const url = (search as string).length > 0 ? URL_SEARCH : URL_CATEGORY;

    const body = {
        "includes": {
            "product": ["id", "name", "description", "translated", "calculatedPrice"],
            "calculated_price": ["totalPrice"]
        },
        "search": search,
        "order": sort
    };

    try {
        const data = await $fetch.raw(url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'sw-access-key': config.API_KEY,
            },
            body: JSON.stringify(body)
        });

        // @ts-ignore
        return mapperProducts(data._data.elements);
    } catch (error) {
        throw createError({ statusCode: 404, statusMessage: 'Page Not Found' })
    }
});
